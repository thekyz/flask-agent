from flask import Flask, jsonify, request

from broker.model.node import Node, NodeSchema
from broker.model.service import Service, ServiceSchema
from broker.model.os_type import OsType

app = Flask(__name__)

all_nodes = [
    Node('127.0.0.1', 6001, 'pegasus', OsType.DARWIN, [Service('nxh3770')]),
    Node('127.0.0.1', 6002, 'perseus', OsType.LINUX, [Service('nxh3770'), Service('nxh3670')]),
]

@app.route('/nodes')
def get_nodes():
    schema = NodeSchema(many=True)
    nodes = schema.dump(all_nodes)
    return jsonify(nodes.data)

@app.route('/nodes', methods=['POST'])
def add_node():
    node = NodeSchema().load(request.get_json())
    all_nodes.append(node.data)
    return '', 204

if __name__ == '__main__':
    app.run()
