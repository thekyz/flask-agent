from enum import Enum

class OsType(Enum):
    DARWIN = 'DARWIN'
    WIN7 = 'WIN7'
    WIN10 = 'WIN10'
    LINUX = 'LINUX'
