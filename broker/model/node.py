import datetime as dt

from marshmallow import Schema, fields, post_load

from .service import Service, ServiceSchema

class Node():
    def __init__(self, ip, port, name, os, services):
        self.ip = ip
        self.port = port
        self.name = name
        self.os = os
        self.services = services
        self.added_at = dt.datetime.now()

    def __repr__(self):
        return '<Node(name={self.name!r}}>'.format(self=self)

class NodeSchema(Schema):
    ip = fields.Str(required=True)
    port = fields.Int(required=True)
    name = fields.Str(required=True)
    added_at = fields.Date()
    services = fields.Nested(ServiceSchema, many=True)

    @post_load
    def make_node(self, data):
        return Node(**data)
