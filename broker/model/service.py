from marshmallow import Schema, fields, post_load

class Service():
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Service(name={self.name!r}}>'.format(self=self)

class ServiceSchema(Schema):
    name = fields.Str(required=True)

    @post_load
    def make_service(self, data):
        return Service(**data)
